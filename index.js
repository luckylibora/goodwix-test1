/**
 * Created by lucky on 01.11.16.
 */

var fs = require('fs');
var http = require('http');
var path = require('path');
var config = require('./config');
var mimeTypes = require('./mimeTypes');


/**
 *
 * @param {Error} err
 * @param {ServerResponse} res
 */
function onError(err, res) {
    console.error(err.message);
    var errorCodes = {
        'ENOENT': 404
    };
    var defaultCode = 500;
    var code = errorCodes[err.code] || defaultCode;
    res.setHeader('Content-Type', 'text/plain; charset=UTF-8');
    res.setHeader('Content-Length', Buffer.byteLength(err.message));
    res.writeHead(code);
    res.end(err.message);
}


/**
 *
 * @param {IncomingMessage} req
 * @returns {Date | null}
 */
function getIfModifiedSinceHeader(req) {
    var header = req.headers['if-modified-since'];
    return header ? new Date(header) : null;
}


/**
 *
 * @param {string} filePath
 * @param {IncomingMessage} req
 * @param {ServerResponse} res
 */
function sendFile(filePath, req, res) {
    fs.stat(filePath, function (err, stat) {
        res.setHeader('Accept-Ranges', 'bytes');

        if (err) {
            return onError(err, res);
        }

        if (!stat.isFile()) {
            return onError(new Error('Accessing directories is no permitted'), res);
        }

        res.setHeader('Last-Modified', stat.mtime);

        var ifModifiedSince = getIfModifiedSinceHeader(req);
        if (ifModifiedSince && ifModifiedSince <= stat.mtime) {
            res.writeHead(304);
            return res.end();
        }

        res.setHeader('Content-Length', stat.size);


        var mimeType = mimeTypes.getMimeType(filePath);
        if (mimeType) {
            res.setHeader('Content-Type', mimeType);
        }

        var readStream = fs.createReadStream(filePath);

        readStream.on('open', function () {
            readStream.pipe(res);
        }).on('error', function (err) {
            onError(err, res);
        });
    });
}


var server = http.createServer(function (req, res) {
    console.info(req.method, req.url);
    if (req.method != 'GET' && req.method != 'HEAD') {
        return onError(new Error('Not valid http method'), res);
    }
    var fileUrl = decodeURI(req.url.substring(1));
    var filePath = path.join(__dirname, config.staticDir, fileUrl);
    sendFile(filePath, req, res);
});


server.listen(config.port);
console.info('Start listening on ' + config.port);


server.on('error', function (err) {
    console.error(err);
});


process.on('uncaughtException', function (err) {
    console.log(err);
});

