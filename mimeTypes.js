/**
 * Created by lucky on 01.11.16.
 */

var path = require('path');


var mimeTypes = {
    html: "text/html",
    jpeg: "image/jpeg",
    jpg: "image/jpeg",
    png: "image/png",
    js: "application/javascript",
    css: "text/css"
};



/**
 *
 * @param {string} filePath
 * @returns {string | null}
 */
function getMimeType(filePath) {
    var extname = path.extname(filePath);
    if (extname.length <= 1) {
        return null;
    }
    return mimeTypes[extname.substring(1)];
}


exports.getMimeType = getMimeType;