# README #

Simple Node.js static web-server


No need to install any dependencies, all used modules are in Node.js core


##Running##


```
#!bash

node index.js
```

##Config##

Config can be setup via environment

```

STATIC_DIR  - relative path to directory with static files (default 'public')

PORT - listening port (default 3000)

```

##Testing##

Bootstrap and Angular are included in public dir for test. Run server and try to access [http://localhost:3000/angular/angular.js](http://localhost:3000/angular/angular.js)